import React, { Profiler } from 'react';

import Directory from '../../components/directory/directory';

import { HomePageContainer } from './home.styles';

const HomePage = ({ history }) => (
  <HomePageContainer>
    <Profiler
      id="Directory"
      onRender={(id, phase, actualDuration) => {
        console.log({
          id,
          phase,
          actualDuration
        });
      }}
    >

    </Profiler>
    <Directory history={history} />
  </HomePageContainer>
);

export default HomePage;

