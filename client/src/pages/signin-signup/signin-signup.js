import React from 'react';

import SignIn from '../../components/signin/signin';
import SignUp from '../../components/signup/signup';

import './signin-signup.scss';

const SigninSingup = () => (
  <div className='signin-signup'>
    <SignIn />
    <SignUp />
  </div>
);

export default SigninSingup;